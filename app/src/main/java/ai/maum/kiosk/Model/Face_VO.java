package ai.maum.kiosk.Model;

import lombok.Data;

@Data
public class Face_VO {
    private String face_id;
    private String image_path;
    private String repository;
}
