package ai.maum.kiosk.Database;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import ai.maum.kiosk.Common.GlobalVar;
import ai.maum.kiosk.Model.Face_VO;

public class FaceTable {
    public static final String TABLE_NAME = "FACE_TBL";
    public static final String FIELD___ID = "_id";
    public static final String FIELD___FACE_ID = "face_id";
    public static final String FIELD___IMAGE_PATH = "image_path";
    public static final String FIELD___REPOSITORY = "repository";

    public static final String SQL___CREATE___TABLE =
            "CREATE TABLE " + TABLE_NAME + "( " +
            FIELD___ID + " integer PRIMARY KEY autoincrement" + ", " +
            FIELD___FACE_ID + " text " + ", " +
            FIELD___IMAGE_PATH + " text" + ", " +
            FIELD___REPOSITORY + " text" +
            ")";

    public static final String SQL___CREATE___INDEX1 =
            "CREATE INDEX " + TABLE_NAME + "_IDX1 ON " + TABLE_NAME + " (" +
            FIELD___REPOSITORY + ", " + FIELD___FACE_ID +
            ")";


    public static void create(SQLiteDatabase db) {
        db.execSQL(SQL___CREATE___TABLE);
        db.execSQL(SQL___CREATE___INDEX1);
    }

    public static void insert(Face_VO face) {
        String sql = "INSERT INTO " + TABLE_NAME + " (" +
            FIELD___FACE_ID + "," +
            FIELD___IMAGE_PATH + "," +
            FIELD___REPOSITORY + ") VALUES ( ?, ?, ? )";

        Object[] params = {face.getFace_id(), face.getImage_path(), face.getRepository()};
        GlobalVar.mDatabase.execSQL(sql, params);
    }

    public static void delete(String face_id, String repository) {
        String sql = "DELETE FROM " + TABLE_NAME + " WHERE " +
                FIELD___REPOSITORY + "=?" + " AND " +
                FIELD___FACE_ID + "=?";

        Object[] params = {repository, face_id};
        GlobalVar.mDatabase.execSQL(sql, params);
    }

    public static List<Face_VO> getList(String repository, int page_cnt) {
        ArrayList<Face_VO> list = new ArrayList<Face_VO>();

        String sql = "SELECT * FROM " + TABLE_NAME + " WHERE " + FIELD___REPOSITORY + "=? ORDER BY " + FIELD___FACE_ID + " LIMIT " + page_cnt;
        Cursor cur = GlobalVar.mDatabase.query(TABLE_NAME, new String[] { repository }, null, null, null, null, null);
        while(cur.moveToNext()) {
            Face_VO item = new Face_VO();
            item.setFace_id(cur.getString(1));
            item.setImage_path(cur.getString(2));
            item.setRepository(cur.getString(3));

            list.add(item);
        }

        return list;
    }

    public static List<Face_VO> getList(String repository, String face_id, int page_cnt) {
        ArrayList<Face_VO> list = new ArrayList<Face_VO>();

        String sql = "SELECT * FROM " + TABLE_NAME + " WHERE " + FIELD___REPOSITORY + "=? AND " + FIELD___FACE_ID + ">? ORDER BY " + FIELD___FACE_ID + " LIMIT " + page_cnt;
        Cursor cur = GlobalVar.mDatabase.query(TABLE_NAME, new String[] { repository, face_id }, null, null, null, null, null);
        while(cur.moveToNext()) {
            Face_VO item = new Face_VO();
            item.setFace_id(cur.getString(1));
            item.setImage_path(cur.getString(2));
            item.setRepository(cur.getString(3));

            list.add(item);
        }

        return list;
    }

}
