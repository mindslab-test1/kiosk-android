package ai.maum.kiosk.Common;

import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;

import com.tzutalin.dlib.FaceDet;

public class GlobalVar {

    /* DATABASE */
    public static SQLiteDatabase mDatabase;

    /* FACE DETECTOR */
    public static FaceDet mFaceDet = null;

    /* CAPTURE IMAGE */
    public static Bitmap mCaptureBitmap = null;

    /* FACE 저장소 */
    public static String mRepository = "kiosk-test1";
}
