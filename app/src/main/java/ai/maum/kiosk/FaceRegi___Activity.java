package ai.maum.kiosk;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.thekhaeng.pushdownanim.PushDownAnim;
import com.tzutalin.dlib.FaceDet;
import com.tzutalin.dlib.VisionDetRet;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.UUID;

import ai.maum.kiosk.Common.GlobalVar;
import ai.maum.kiosk.Database.FaceTable;
import ai.maum.kiosk.Model.Face_VO;
import ai.maum.kiosk.RestAPI.FaceRecog_Response_DTO;
import ai.maum.kiosk.RestAPI.FaceRecog_RestAPI;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kr.jblab.jbDialog.jbDialog;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Intent.FLAG_ACTIVITY_MULTIPLE_TASK;

public class FaceRegi___Activity extends AppCompatActivity {

    private File mImageFile = null;
    private jbDialog mDialog;
    private jbDialog mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.face_regi___activity);
        ButterKnife.bind(this);

        initDialog();
        initImage();
        initButtons();
        initEdit();
        initRestAPI();
    }

    @Override
    public void onPause() {
        super.onPause();

        if(mImageFile != null) mImageFile.delete();
    }

    /* ============================================================================================= */
    // 다이얼로그 설정
    /* ============================================================================================= */

    private void initDialog() {
        mDialog = jbDialog.builder(this).setTitle("얼굴 등록");
        mProgress = jbDialog.builder(this).setProgress(true).setMessage("얼굴 정보를 등록중입니다....");
    }

    private void showRegiSuccDialog() {
        mDialog
                .setIcon(R.drawable.ic___state___succ___black, R.color.DEFAULT, R.color.SUCCESS)
                .setMessage("얼굴 등록 완료되었습니다.")
                .setButton1("계속 등록", R.color.SUCCESS_TEXT, R.color.SUCCESS, (view)->{
                    finish();

                    Intent it = new Intent(this, CameraCapture___Activity.class);
                    it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | FLAG_ACTIVITY_MULTIPLE_TASK);
                    startActivity(it);
                })
                .setButton2("그만 하기", R.color.PRIMARY_TEXT, R.color.PRIMARY, (view)->{
                    finish();
                })
                .show();
    }


    private void showRegiFailDialog(String message) {
        mDialog
                .setIcon(R.drawable.ic___state___warn___black, R.color.DEFAULT, R.color.DANGER)
                .setMessage(message)
                .setButton1("계속 등록", R.color.SUCCESS_TEXT, R.color.SUCCESS, (view)->{
                    finish();

                    Intent it = new Intent(this, CameraCapture___Activity.class);
                    it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | FLAG_ACTIVITY_MULTIPLE_TASK);
                    startActivity(it);
                })
                .setButton2("그만 하기", R.color.PRIMARY_TEXT, R.color.PRIMARY, (view)->{
                    finish();
                })
                .show();
    }


    /* ============================================================================================= */
    // 이미지 설정
    /* ============================================================================================= */

    @BindView(R.id.ImageView) ImageView mImageView;

    private void initImage() {
        OutputStream out = null;
        String dir = Environment.getExternalStorageDirectory().toString() + "/" + this.getPackageName() + "/Repository/" + GlobalVar.mRepository + "/";
        mImageFile = new File(dir);
        mImageFile.mkdirs();
        mImageFile = new File(dir + UUID.randomUUID().toString() + ".jpg");

        try {
            mImageFile.createNewFile();
            out = new FileOutputStream(mImageFile);
            GlobalVar.mCaptureBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
        } catch(Exception e) {
            mDialog
                    .setIcon(R.drawable.ic___state___error___black, R.color.DEFAULT, R.color.DANGER)
                    .setMessage("이미지를 파일로 저장할 수 없습니다.")
                    .setButton1("확인", R.color.DANGER_TEXT, R.color.DANGER, (view)->{
                        finish();
                    })
                    .show();
        }

        mImageView.setImageBitmap(GlobalVar.mCaptureBitmap);
    }


    /* ============================================================================================= */
    // EDIT 설정
    /* ============================================================================================= */

    @BindView(R.id.Edit_FaceId) EditText mEdit_FaceId;

    private void initEdit() {
        mEdit_FaceId.addTextChangedListener(new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {          }
            @Override public void afterTextChanged(Editable s) {         }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.e("AAA", "text = " + s);

                if(mEdit_FaceId.getText().toString().trim().length() == 0) mButton_Regi.setEnabled(false);
                else mButton_Regi.setEnabled(true);
            }


        });
    }


    /* ============================================================================================= */
    // 버튼 설정
    /* ============================================================================================= */

    @BindView(R.id.Button_Regi) TextView mButton_Regi;
    @BindView(R.id.Button_Recapture) TextView mButton_Recapture;
    @BindView(R.id.Button_Close) TextView mButton_Close;

    private void initButtons() {
        PushDownAnim.setPushDownAnimTo(mButton_Regi, mButton_Recapture, mButton_Close);
    }

    @OnClick(R.id.Button_Regi)
    public void onClick_Regi() {
        regiRaceToServer();
    }

    @OnClick(R.id.Button_Recapture)
    public void onClick_Recapture() {
        Intent it = new Intent(this, CameraCapture___Activity.class);
        it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | FLAG_ACTIVITY_MULTIPLE_TASK);
        startActivity(it);
    }

    @OnClick(R.id.Button_Close)
    public void onClick_Close() {
        finish();
    }



    /* ============================================================================================= */
    // 등록
    /* ============================================================================================= */

    FaceRecog_RestAPI mRestAPI;

    private void initRestAPI() {
        mRestAPI = new FaceRecog_RestAPI();
    }

    private void  regiRaceToServer() {
        mProgress.show();

        RequestBody id = RequestBody.create(MediaType.parse("text/plain"), getString(R.string.api_id));
        RequestBody key = RequestBody.create(MediaType.parse("text/plain"), getString(R.string.api_key));
        RequestBody db_id = RequestBody.create(MediaType.parse("text/plain"), GlobalVar.mRepository);
        RequestBody face_id = RequestBody.create(MediaType.parse("text/plain"), mEdit_FaceId.getText().toString().trim());

        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), mImageFile);
        MultipartBody.Part uploadFile = MultipartBody.Part.createFormData("file", mImageFile.getName(), requestFile);

        Call<FaceRecog_Response_DTO> call = mRestAPI.face.setFace( id, key, db_id, face_id, uploadFile );
        call.enqueue(new Callback<FaceRecog_Response_DTO>() {
            @Override
            public void onResponse(Call<FaceRecog_Response_DTO> call, Response<FaceRecog_Response_DTO> response) {
                mProgress.dismiss();

                if (response.isSuccessful()) {
                    if(response.body().getMessage().getStatus() == 0) {
                        Face_VO face = new Face_VO();
                        face.setFace_id(mEdit_FaceId.getText().toString());
                        face.setRepository(GlobalVar.mRepository);
                        face.setImage_path(mImageFile.getAbsolutePath());
                        FaceTable.insert(face);

                        mImageFile = null;
                        showRegiSuccDialog();
                    }
                    else {
                        showRegiFailDialog("얼굴 등록 실패: " + response.body().getMessage().getMessage());
                    }
                }
            };

            @Override
            public void onFailure(Call<FaceRecog_Response_DTO> call, Throwable t) {
                mProgress.dismiss();

                showRegiFailDialog("얼굴 등록 실패: 서버와 연동이 되지 않습니다.");
            }
        });
    }
}
