package ai.maum.kiosk;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.widget.TextView;
import android.widget.Toast;

import com.thekhaeng.pushdownanim.PushDownAnim;
import com.tzutalin.dlib.FaceDet;
import com.tzutalin.dlib.VisionDetRet;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.List;

import ai.maum.kiosk.Common.GlobalVar;
import ai.maum.kiosk.Database.DatabaseHelper;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kr.jblab.jbDialog.jbDialog;

import static android.content.Intent.FLAG_ACTIVITY_MULTIPLE_TASK;

public class Home___Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home___activity);
        ButterKnife.bind(this);

        initDatabase();
        initButtons();
        initPermission();
    }


    /* ============================================================================================= */
    // 데이타베이스 설정
    /* ============================================================================================= */

    private void initDatabase() {
        DatabaseHelper helper = new DatabaseHelper(this);
        GlobalVar.mDatabase = helper.getWritableDatabase();
    }


    /* ============================================================================================= */
    // 버튼 설정
    /* ============================================================================================= */

    @BindView(R.id.Button_GoService) TextView mButton_Service;
    @BindView(R.id.Button_FaceRegi) TextView mButton_Regi;
    @BindView(R.id.Button_Setup) TextView mButton_Setup;
    private void initButtons() {
        PushDownAnim.setPushDownAnimTo(mButton_Service, mButton_Regi, mButton_Setup);
    }

    @OnClick(R.id.Button_GoService)
    public void onClick_Service() {
        Intent it = new Intent(this, Service___Activity.class);
        it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | FLAG_ACTIVITY_MULTIPLE_TASK);
        startActivity(it);
    }

    @OnClick(R.id.Button_FaceRegi)
    public void onClick_FaceRegi() {
        Intent it = new Intent(this, CameraCapture___Activity.class);
        it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | FLAG_ACTIVITY_MULTIPLE_TASK);
        startActivity(it);
    }

    @OnClick(R.id.Button_Setup)
    public void onClick_Setup() {

    }


    /* ============================================================================================= */
    // 메세지 처리
    /* ============================================================================================= */

    private final int MSG_ID____COMPLETED___INIT_FACE_DETECTION = 10;

    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch(msg.what) {
                case MSG_ID____COMPLETED___INIT_FACE_DETECTION:
                    mDialog_Progress.dismiss();
                    break;
            }
        }
    };


    /* ============================================================================================= */
    // 서비스 초기화
    /* ============================================================================================= */

    jbDialog mDialog_Progress;
    private String PATH___DLIB_LANDMARK_DAT;

    private void initService() {
        mDialog_Progress = jbDialog.builder(this)
                .setProgress(true)
                .setMessage("서비스 초기화 작업중입니다.");
        mDialog_Progress.show();

        initFaceDetector();
    }

    private void initFaceDetector() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                copyLandmarkData();
                if(GlobalVar.mFaceDet == null) {
                    GlobalVar.mFaceDet = new FaceDet(PATH___DLIB_LANDMARK_DAT);
                    Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.face_recognition___sample);
                    List<VisionDetRet> results = GlobalVar.mFaceDet.detect(bitmap);
                }
                mHandler.sendEmptyMessage(MSG_ID____COMPLETED___INIT_FACE_DETECTION);
            }
        }).start();
    }


    /*
     ** DLIB의 랜드마크 데이터 파일을 특정 폴더로 복사
     */
    void copyLandmarkData() {
        byte [] buffer = new byte[8*1024];
        int length = 0;

        PATH___DLIB_LANDMARK_DAT = Environment.getExternalStorageDirectory().toString() + "/" + this.getPackageName();
        File file = new File(PATH___DLIB_LANDMARK_DAT);
        file.mkdirs();

        PATH___DLIB_LANDMARK_DAT += "/shape_predictor_68_face_landmarks.dat";
        file = new File(PATH___DLIB_LANDMARK_DAT);
        if(file.exists()) return;

        try {
            InputStream is = getResources().openRawResource(R.raw.shape_predictor_68_face_landmarks);
            BufferedInputStream bis = new BufferedInputStream(is);
            FileOutputStream fos = new FileOutputStream(PATH___DLIB_LANDMARK_DAT);

            while ((length = bis.read(buffer)) >= 0) fos.write(buffer, 0, length);
            fos.flush();
            fos.close();
        } catch (Exception e) {
            Toast.makeText(this, "데이타 파일 복사가 실패하였습니다.", Toast.LENGTH_LONG).show();
        }
    }

    /* ============================================================================================= */
    // 권한 설정
    /* ============================================================================================= */

    private int REQUEST_CODE_PERMISSIONS = 10; //arbitrary number, can be changed accordingly
    private final String[] REQUIRED_PERMISSIONS = new String[]{
            "android.permission.CAMERA",
            "android.permission.RECORD_AUDIO",
            "android.permission.WRITE_EXTERNAL_STORAGE"};

    private void initPermission() {
        if(allPermissionsGranted()){
            initService();
        } else{
            ActivityCompat.requestPermissions(this, REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSIONS);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //start camera when permissions have been granted otherwise exit app
        if(requestCode == REQUEST_CODE_PERMISSIONS){
            if(allPermissionsGranted()){
                initService();
            } else{
                Toast.makeText(this, "Permissions not granted by the user.", Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    private boolean allPermissionsGranted(){
        //check if req permissions have been granted
        for(String permission : REQUIRED_PERMISSIONS){
            if(ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED){
                return false;
            }
        }
        return true;
    }
}
