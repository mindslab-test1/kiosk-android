package ai.maum.kiosk;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.ImageProxy;
import androidx.core.app.ActivityCompat;

import com.tzutalin.dlib.FaceDet;
import com.tzutalin.dlib.VisionDetRet;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import ai.maum.kiosk.Common.GlobalVar;
import ai.maum.kiosk.RestAPI.FaceRecog_Response_DTO;
import ai.maum.kiosk.RestAPI.FaceRecog_RestAPI;
import butterknife.BindView;
import butterknife.ButterKnife;
import fr.castorflex.android.circularprogressbar.CircularProgressBar;
import kr.jblab.jbCamera1.jbCameraView;
import kr.jblab.jbDialog.jbDialog;
import kr.jblab.jbDialog.jbMessageBox;
import kr.jblab.jbFaceRecog.jbFacePosGuideView;
import kr.jblab.jbFaceRecog.jbFaceWireFrameView;
import kr.jblab.jbUtils.jbUtils;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Intent.FLAG_ACTIVITY_MULTIPLE_TASK;

public class Service___Activity extends AppCompatActivity {

    private final int COLOR___WHITE = 0xFFFFFFFF;
    private final int COLOR___RED = 0xFFFF6060;
    private final int COLOR___YELLOW = 0xFFe7e253;

    private final int MSG_ID____COMPLETED___INIT_FACE_DETECTION = 10;
    private final int MSG_ID____SUCC___RECOGNITION_FACE = 11;
    private final int MSG_ID____FAIL___RECOGNITION_FACE = 12;
    private final int MSG_ID____FAIL___SERVER = 13;

    private final int CENTER___WIDTH_DELTA = 60; /* dp */
    private final float CENTER___HEIGHT_DELTA_TOP_RATE = 2/8f; /* 비율 */
    private final float CENTER___HEIGHT_DELTA_BOTTOM_RATE = 5/8f; /* 비율 */

    private final float SCALE___MIN = 0.4f;
    private final float SCALE___MAX = 0.7f;

    private final float SCALE___FACE_DETECT = 4f; // 얼굴 인식을 위해 Bitmap 축소 비율 (bitmap size / rate).

    private final int MAX_MOVING_COUNG = 2;
    private final int MAX_MOVING_SUM = 10;

    ImageView mImageView_Preview;

    @BindView(R.id.CameraView)
    jbCameraView mCameraView;
    @BindView(R.id.Progress)
    CircularProgressBar mProgress;

    jbMessageBox mMessageBox;
    jbDialog mDialog_Progress;

    TextView mTextView_Comment;
    jbFacePosGuideView mFaceGuide;

    jbFaceWireFrameView mFaceWireFrame;

    FaceRecog_RestAPI mRestAPI;

    boolean mFlag_RecognizingFace = false;



    /* ============================================================================================= */
    // Activity 라이프 사이클
    /* ============================================================================================= */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        setContentView(R.layout.service___activity);
        ButterKnife.bind(this);

        mCameraView.setMode(mCameraView.MODE_ANALISIS);
        mCameraView.setAnalyzer(mAnalyzer);

        mImageView_Preview = (ImageView) findViewById(R.id.ImageView_Preview);
        mTextView_Comment = (TextView) findViewById(R.id.TextView_Comment);
        mFaceGuide = (jbFacePosGuideView) findViewById(R.id.FaceGuide);
        mFaceGuide.setColor(COLOR___RED);
        mFaceWireFrame = (jbFaceWireFrameView) findViewById(R.id.FaceWireFrame);

        mRestAPI = new FaceRecog_RestAPI();
        

        /* 팝업 생성 */
        mMessageBox = jbMessageBox.build( this );
    }

    @Override
    public void onResume() {
        super.onResume();

        mFaceWireFrame.setVisibility(View.GONE);
        mCameraView.startCamera(this);
    }

    @Override
    public void onPause() {
        super.onPause();

        mCameraView.stopCamera();
    }


    /* ============================================================================================= */
    // 메세지 핸들러
    /* ============================================================================================= */

    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Intent it = null;

            switch(msg.what) {

                case MSG_ID____SUCC___RECOGNITION_FACE:
                    Log.e("AAA", "오 얼굴이 식별되었습니다. - " + (String) msg.obj);

                    it = new Intent(Service___Activity.this, Match___Activity.class);
                    it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | FLAG_ACTIVITY_MULTIPLE_TASK);
                    startActivity(it);
                    finish();

                    break;

                case MSG_ID____FAIL___RECOGNITION_FACE:
                    Log.e("AAA", "사진의 얼굴을 찾을 수가 없습니다.");

//                    jbDialog.builder(Service___Activity.this)
//                            .setTitle("얼굴 식별")
//                            .setIcon(R.drawable.ic___state___warn___black, R.color.DEFAULT, R.color.DANGER)
//                            .setMessage("고객님의 얼굴과 매칭되는 정보가 없습니다.")
//                            .setButton1("다시하기", R.color.PRIMARY_TEXT, R.color.PRIMARY, (view)->{
//                                mFlag_RecognizingFace = false;
//                                mFaceWireFrame.setLandmark(null, 0, null, null);
//                                mCameraView.startCamera(Service___Activity.this);
//                            })
//                            .setButton2("취소", R.color.DEFAULT_TEXT, R.color.DEFAULT, (view)->{
//                                finish();
//                            })
//                            .show();

                    it = new Intent(Service___Activity.this, Mismatch___Activity.class);
                    it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | FLAG_ACTIVITY_MULTIPLE_TASK);
                    startActivity(it);
                    finish();
                    break;

                case MSG_ID____FAIL___SERVER:
                    Log.e("AAA", "서버 연동 실패");

                    jbMessageBox.build(Service___Activity.this)
                            .setMessage("서버와 연결이 되지 않습니다.")
                            .setAccept("다시하기", (view) -> {
                                mFlag_RecognizingFace = false;

                                mFaceWireFrame.setVisibility(View.GONE);
                                mFaceWireFrame.setLandmark(null, 0, null, null);
                                mCameraView.startCamera(Service___Activity.this);
                            })
                            .setCancel("취소", (view) -> {
                                finish();
                            })
                            .show();

                    break;
            }
        }
    };



    /* ============================================================================================= */
    // 얼굴 인식
    /* ============================================================================================= */

    private double mMovingSum = 0;    // 움직임 감지 값. (3연속 얼굴 중심점이 변화량).
    private Point mPoint_BeforeFace = new Point(0, 0);
    private int mMovingCount = 0;


    void detectFace(Bitmap bitmap) {

        /* 현재 얼굴 식별 중이면 바로 리턴 */
        if(mFlag_RecognizingFace) {
            mCameraView.stopCamera();
            return;
        }

        ArrayList<Point> landmarks = null;
        List<VisionDetRet> results = GlobalVar.mFaceDet.detect(bitmap);
        for(VisionDetRet ret : results) {
            ArrayList<Point> landmarks_tmp = ret.getFaceLandmarks();
            if(landmarks == null) landmarks = landmarks_tmp;
            else if(landmarks_tmp.get(16).x - landmarks_tmp.get(0).x > landmarks.get(16).x - landmarks.get(0).x ) landmarks = landmarks_tmp;
        }
        if(landmarks == null) {
            Service___Activity.this.runOnUiThread(()->{
                mTextView_Comment.setText("정면을 바라보세요.");
                mFaceGuide.setColor(COLOR___RED);
            });

            mPoint_BeforeFace = new Point(0, 0);
            mMovingSum = 0;
            mMovingCount = 0;
            return;
        }

        /* 얼굴의 좌우 위치 조정하게 해준다. */
        if(checkLeftDirection(calcViewX(landmarks.get(27).x)) == false
                || checkRightDirection(calcViewX(landmarks.get(27).x)) == false) {

            if(checkLeftDirection(calcViewX(landmarks.get(27).x)) == false) {
                Service___Activity.this.runOnUiThread(() -> {
                    mTextView_Comment.setText("얼굴을 오른쪽 중앙에 위치하도록 조정해주세요.");
                    mFaceGuide.setColor(COLOR___RED);
                });
            } else {
                Service___Activity.this.runOnUiThread(() -> {
                    mTextView_Comment.setText("얼굴을 왼쪽 중앙에 위치하도록 조정해주세요.");
                    mFaceGuide.setColor(COLOR___RED);
                });
            }

            mPoint_BeforeFace = new Point(0, 0);
            mMovingSum = 0;
            mMovingCount = 0;
            return;
        }

        /* 얼굴의 상하 위치 조정하게 해준다. */
        if(checkUpDirection(calcViewY(landmarks.get(27).y)) == false
                || checkDownDirection(calcViewY(landmarks.get(27).y)) == false) {

            if(checkUpDirection(calcViewY(landmarks.get(27).y)) == false) {
                Service___Activity.this.runOnUiThread(() -> {
                    mTextView_Comment.setText("얼굴을 아래쪽 중앙에 위치하도록 조정해주세요.");
                    mFaceGuide.setColor(COLOR___RED);
                });
            } else {
                Service___Activity.this.runOnUiThread(() -> {
                    mTextView_Comment.setText("얼굴을 위쪽 중앙에 위치하도록 조정해주세요.");
                    mFaceGuide.setColor(COLOR___RED);
                });
            }

            mPoint_BeforeFace = new Point(0, 0);
            mMovingSum = 0;
            mMovingCount = 0;
            return;
        }

        /* 카메라와의 거리를 조정하게 해준다. */
        if(checkScaleUp(calcViewX(landmarks.get(16).x) - calcViewX(landmarks.get(0).x)) == false) {
            Service___Activity.this.runOnUiThread(()->{
                mTextView_Comment.setText("조금 더 멀어 지세요.");
                mFaceGuide.setColor(COLOR___RED);
            });

            mPoint_BeforeFace = new Point(0, 0);
            mMovingSum = 0;
            mMovingCount = 0;
            return;
        } else if(checkScaleDown(calcViewX(landmarks.get(16).x) - calcViewX(landmarks.get(0).x)) == false) {
            Service___Activity.this.runOnUiThread(()->{
                mTextView_Comment.setText("조금 더 가까이 오세요.");
                mFaceGuide.setColor(COLOR___RED);
            });

            mPoint_BeforeFace = new Point(0, 0);
            mMovingSum = 0;
            mMovingCount = 0;
            return;
        }

        Service___Activity.this.runOnUiThread(()->{
            mTextView_Comment.setText("가만히 계시면 인식 시작합니다.");
            mFaceGuide.setColor(COLOR___WHITE);
        });

        mMovingSum += jbUtils.getDistance(mPoint_BeforeFace.x, mPoint_BeforeFace.y, landmarks.get(27).x, landmarks.get(27).y);
        mMovingCount++;
        mPoint_BeforeFace = new Point(landmarks.get(27).x, landmarks.get(27).y);

        Log.e("AAA", "Moving Count = " + mMovingCount);

        if(mMovingCount < MAX_MOVING_COUNG) return;
        else if(mMovingCount == MAX_MOVING_COUNG && mMovingSum > MAX_MOVING_SUM) {
            Log.e("AAA", "MovingSum = " + mMovingSum);
            mMovingCount = 0;
            mMovingSum = 0;
            return;
        }
        Log.e("AAA", "Start Recognition ..........");

        Service___Activity.this.runOnUiThread(()->{
            mProgress.setVisibility(View.VISIBLE);
            mTextView_Comment.setText("얼굴 식별중입니다...");
//            mFaceGuide.setColor(COLOR___YELLOW);
        });

        final ArrayList<Point> tmpLandmarks = landmarks;
        Service___Activity.this.runOnUiThread(()-> {
            mCameraView.stopCamera();
            mFaceWireFrame.setVisibility(View.VISIBLE);
            mFaceWireFrame.setLandmark(tmpLandmarks, 4, mCameraView.getPreviewOffset(), mCameraView.getPreviewScale());

        });
        mFlag_RecognizingFace = true;

        String folder = Environment.getExternalStorageDirectory().toString() + "/" + this.getPackageName() + "/FaceRecognition/";
        jbUtils.saveBitmaptoJpeg(bitmap, folder, "myface.jpg");
        recognizeFace(folder + "myface.jpg");

    }

    /*
     ** 위쪽 방향으로 경계를 넘었는지 확인. (true: 정상)
     */
    private boolean checkUpDirection(int y) {
        float adjustY = y - mFaceGuide.getY();

        if(adjustY < mFaceGuide.getHeight() * CENTER___HEIGHT_DELTA_TOP_RATE) return false;
        return true;
    }

    /*
     ** 아래쪽 방향으로 경계를 넘었는지 확인. (true: 정상)
     */
    private boolean checkDownDirection(int y) {
        float adjustY = y - mFaceGuide.getY();

        if(adjustY > mFaceGuide.getHeight() * CENTER___HEIGHT_DELTA_BOTTOM_RATE) return false;
        return true;
    }

    /*
     ** 왼쪽 방향으로 경계를 넘었는지 확인. (true: 정상)
     */
    private boolean checkLeftDirection(int x) {
        float adjustX = x - mFaceGuide.getX();
        if(adjustX < mFaceGuide.getWidth()/2 - jbUtils.dp2px(Service___Activity.this, CENTER___WIDTH_DELTA)) return false;
        return true;
    }

    /*
     ** 오른쪽 방향으로 경계를 넘었는지 확인. (true: 정상)
     */
    private boolean checkRightDirection(int x) {
        float adjustX = x - mFaceGuide.getX();
        if(adjustX > mFaceGuide.getWidth()/2 + jbUtils.dp2px(Service___Activity.this, CENTER___WIDTH_DELTA)) return false;
        return true;
    }

    /*
     ** 카메라와의 가까운쪽 거리 경계를 넘었는지 확인. (true: 정상)
     */
    private boolean checkScaleUp(float distance) {
        if(distance > mFaceGuide.getWidth() * SCALE___MAX) return false;
        return true;
    }

    /*
     ** 카메라와의 먼쪽 거리 경계를 넘었는지 확인. (true: 정상)
     */
    private boolean checkScaleDown(float distance) {
        if(distance < mFaceGuide.getWidth() * SCALE___MIN) return false;
        return true;
    }


    /* ============================================================================================= */
    // 얼굴 식별
    /* ============================================================================================= */

    void recognizeFace(String imageFilePath) {

        RequestBody id = RequestBody.create(MediaType.parse("text/plain"), getString(R.string.api_id));
        RequestBody key = RequestBody.create(MediaType.parse("text/plain"), getString(R.string.api_key));
        RequestBody db_id = RequestBody.create(MediaType.parse("text/plain"), GlobalVar.mRepository);

        File file = new File(imageFilePath);
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part uploadFile = MultipartBody.Part.createFormData("file", file.getName(), requestFile);

        Call<FaceRecog_Response_DTO> call = mRestAPI.face.recogFace( id, key, db_id, uploadFile );
        call.enqueue(new Callback<FaceRecog_Response_DTO>() {
            @Override
            public void onResponse(Call<FaceRecog_Response_DTO> call, Response<FaceRecog_Response_DTO> response) {
                mProgress.setVisibility(View.GONE);
                mTextView_Comment.setText("");

                if (response.isSuccessful()) {
                    if(response.body().getMessage().getMessage().equalsIgnoreCase("Success")) {
                        /* 성공 결과 전송 */
                        mHandler.sendMessage(mHandler.obtainMessage(MSG_ID____FAIL___RECOGNITION_FACE, response.body().getResult().getId()));
                    }
                    else {
                        /* 실패 결과 전송 */
                        mHandler.sendEmptyMessage(MSG_ID____FAIL___RECOGNITION_FACE);
                    }
                }
            };

            @Override
            public void onFailure(Call<FaceRecog_Response_DTO> call, Throwable t) {
                mProgress.setVisibility(View.GONE);
                mTextView_Comment.setText("");

                /* 실패 결과 전송 */
                mHandler.sendEmptyMessage(MSG_ID____FAIL___SERVER);
                return;
            }
        });
    }


    /* ============================================================================================= */
    // Landmark 좌표를 View 좌표로 변환
    /* ============================================================================================= */

    int calcViewX(int x) {
        return (int) (x*SCALE___FACE_DETECT * mCameraView.getPreviewScale().getWidth() - mCameraView.getPreviewOffset().getWidth());
    }

    int calcViewY(int y) {
        return (int) (y*SCALE___FACE_DETECT * mCameraView.getPreviewScale().getHeight() - mCameraView.getPreviewOffset().getHeight());
    }



    /* ============================================================================================= */
    //
    /* ============================================================================================= */

    ImageAnalysis.Analyzer mAnalyzer = new ImageAnalysis.Analyzer() {
        @Override
        public void analyze(ImageProxy image, int rotationDegrees) {
            Log.e("AAA", "analyzer ..................... size = " + image.getWidth() + "x" + image.getHeight());

            Bitmap bitmap = jbCameraView.toBitmap(image.getImage());

            Matrix matrix = new Matrix();
            matrix.postRotate(-90);
            matrix.postScale(-1, 1);
            Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, (int) (bitmap.getWidth()/SCALE___FACE_DETECT), (int) (bitmap.getHeight()/SCALE___FACE_DETECT), true);
            Bitmap rotatedBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);

            Service___Activity.this.runOnUiThread(()->{
                mImageView_Preview.setImageBitmap(rotatedBitmap);
            });

            Log.e("AAA", "step...........1.. ");
            detectFace(rotatedBitmap);
            Log.e("AAA", "step...........2");
        }
    };
}
