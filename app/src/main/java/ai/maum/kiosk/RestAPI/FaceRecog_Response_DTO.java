package ai.maum.kiosk.RestAPI;

import java.util.List;

import lombok.Data;

@Data
public class FaceRecog_Response_DTO {
    private Message message;
    private Result result;
    private List<Payload> payload;

    @Data
    public class Message {
        private String message;
        private int status;
    }

    @Data
    public class Result {
        private String id;
    }

    @Data
    public class Payload {
        private String id;
    }
}
