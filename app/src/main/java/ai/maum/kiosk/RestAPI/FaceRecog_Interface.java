package ai.maum.kiosk.RestAPI;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;

public interface FaceRecog_Interface {
    @Multipart
    @POST("/insight/app/recogFace/")
    Call<FaceRecog_Response_DTO> recogFace(
            @Part("apiId") RequestBody apiId,
            @Part("apiKey") RequestBody apiKey,
            @Part("dbId") RequestBody dbId,
            @Part MultipartBody.Part file
    );

    @Multipart
    @PUT("/insight/app/setFace/")
    Call<FaceRecog_Response_DTO> setFace(
            @Part("apiId") RequestBody apiId,
            @Part("apiKey") RequestBody apiKey,
            @Part("dbId") RequestBody dbId,
            @Part("faceId") RequestBody faceId,
            @Part MultipartBody.Part file
    );

    @Multipart
    @POST("/insight/app/getFaceList/")
    Call<FaceRecog_Response_DTO> getFaceList(
            @Part("apiId") RequestBody apiId,
            @Part("apiKey") RequestBody apiKey,
            @Part("dbId") RequestBody dbId
    );

    @Multipart
    @POST("/insight/app/deleteFace/")
    Call<FaceRecog_Response_DTO> deleteFace(
            @Part("apiId") RequestBody apiId,
            @Part("apiKey") RequestBody apiKey,
            @Part("dbId") RequestBody dbId,
            @Part("faceId") RequestBody faceId
    );
}
