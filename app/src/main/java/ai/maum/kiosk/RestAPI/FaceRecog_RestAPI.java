package ai.maum.kiosk.RestAPI;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FaceRecog_RestAPI {
    String URL = "https://api.maum.ai/"; // 서버 API

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    public FaceRecog_Interface face = retrofit.create(FaceRecog_Interface.class);
}
