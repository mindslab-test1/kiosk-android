package ai.maum.kiosk;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.media.FaceDetector;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.camera.core.ImageCapture;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.thekhaeng.pushdownanim.PushDownAnim;
import com.tzutalin.dlib.VisionDetRet;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ai.maum.kiosk.Common.GlobalVar;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kr.jblab.jbCamera1.jbCameraView;
import kr.jblab.jbDialog.jbDialog;

import static ai.maum.kiosk.Common.GlobalVar.mFaceDet;
import static android.content.Intent.FLAG_ACTIVITY_MULTIPLE_TASK;

public class CameraCapture___Activity extends AppCompatActivity {

    @BindView(R.id.Preview) ImageView mPreview;
    @BindView(R.id.CameraView) jbCameraView mCameraView;
    jbDialog mDialog;
    jbDialog mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.camera_capture___activity);
        ButterKnife.bind(this);

        initButtons();

    }

    @Override
    protected void onResume() {
        super.onResume();

        mCameraView.setMode(mCameraView.MODE_CAPTURE);
        mCameraView.startCamera(this);
        initDialog();
    }

    @Override
    protected void onPause() {
        super.onPause();

        mCameraView.stopCamera();
    }


    /* ============================================================================================= */
    // 다이엘로그 설정
    /* ============================================================================================= */

    private void initDialog() {
        mDialog = jbDialog.builder(this);

        mProgress = jbDialog.builder(this)
                .setProgress(true)
                .setTitle("캡쳐")
                .setMessage("이미지 분석중입니다.....");
    }

    /* ============================================================================================= */
    // 버튼 설정
    /* ============================================================================================= */

    @BindView(R.id.Button_Capture) TextView mButton_Capture;

    private void initButtons() {
        PushDownAnim.setPushDownAnimTo(mButton_Capture);
    }

    @OnClick(R.id.Button_Capture)
    public void onClick_Capture() {
        mProgress.show();

        String dir = Environment.getExternalStorageDirectory().toString() + "/" + CameraCapture___Activity.this.getPackageName();
        File file = new File(dir);
        file.mkdirs();
        file = new File(dir + "/capture.jpg");
        file.delete();
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mCameraView.takePicture(file, new ImageCapture.OnImageSavedListener() {
            @Override
            public void onImageSaved(@NonNull File file) {
                Log.e("AAA", "Captured.......");
                mCameraView.stopCamera();

                Glide.with(CameraCapture___Activity.this).asBitmap().load(file).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE)
                        .into(new CustomTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                                Log.e("AAA", "Bitmap Size = " + resource.getWidth() + "x" + resource.getHeight());

                                extractFace(resource);
                            }

                            @Override
                            public void onLoadCleared(@Nullable Drawable placeholder) {

                            }
                        });
            }

            @Override
            public void onError(@NonNull ImageCapture.UseCaseError useCaseError, @NonNull String message, @Nullable Throwable cause) {
                Log.e("AAA", "Capture fail.......");
                mProgress.dismiss();

                mDialog
                        .setIcon(R.drawable.ic___state___info___black, R.color.DEFAULT, R.color.DANGER)
                        .setTitle("캡쳐").setMessage("사진 캡쳐가 실패하였습니다. (" + message + ")")
                        .setButton1("다시하기", R.color.PRIMARY_TEXT, R.color.PRIMARY, (view)->{
                            mCameraView.startCamera(CameraCapture___Activity.this);
                        })
                        .setButton2("취소", R.color.PRIMARY_TEXT, R.color.PRIMARY, (view)->{
                            finish();
                        })
                        .show();
            }
        });
    }


    /* ============================================================================================= */
    // 얼굴 추출
    /* ============================================================================================= */

    private void extractFace(final Bitmap bitmap) {
//        mPreview.setImageBitmap(bitmap);
        new Thread(()->{
            ArrayList < Point > landmarks = null;
            List<VisionDetRet> results = mFaceDet.detect(bitmap);
            for(VisionDetRet ret : results) {
                ArrayList<Point> landmarks_tmp = ret.getFaceLandmarks();
                if(landmarks == null) landmarks = landmarks_tmp;
                else if(landmarks_tmp.get(16).x - landmarks_tmp.get(0).x > landmarks.get(16).x - landmarks.get(0).x ) landmarks = landmarks_tmp;
            }
            if(landmarks == null) {
                Log.e("AAA", "얼굴을 찾을 수가 없어요.");

                CameraCapture___Activity.this.runOnUiThread(()->{
                    mProgress.dismiss();
                    mDialog
                            .setIcon(R.drawable.ic___state___info___black, R.color.DEFAULT, R.color.DANGER)
                            .setTitle("캡쳐").setMessage("캡쳐된 사진에서 얼굴을 찾을 수가 없습니다.")
                            .setButton1("다시하기", R.color.PRIMARY_TEXT, R.color.PRIMARY, (view)->{
                                mCameraView.startCamera(CameraCapture___Activity.this);
                            })
                            .setButton2("그만하기", R.color.PRIMARY_TEXT, R.color.PRIMARY, (view)->{
                                finish();
                            })
                            .show();
                });

            }
            else {
                Log.e("AAA", "얼굴을 찾았어요. distance = " + (landmarks.get(16).x - landmarks.get(0).x));


                    final ArrayList<Point> landmarksTmp = landmarks;
                    CameraCapture___Activity.this.runOnUiThread(() -> {
                        try {

                            int face_width = landmarksTmp.get(16).x - landmarksTmp.get(0).x;
                            int face_height = landmarksTmp.get(8).y - landmarksTmp.get(0).y;
                            Bitmap detectedBitmap = Bitmap.createBitmap(bitmap, landmarksTmp.get(0).x - face_width / 2, (int) (landmarksTmp.get(0).y - face_height * 1.5f), face_width * 2, face_height * 3);
                            GlobalVar.mCaptureBitmap = detectedBitmap;
                            Intent it = new Intent(CameraCapture___Activity.this, FaceRegi___Activity.class);
                            it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | FLAG_ACTIVITY_MULTIPLE_TASK);
                            startActivity(it);

                            mProgress.dismiss();
                            finish();
                        } catch(Exception e) {
                            mProgress.dismiss();
                            mDialog
                                    .setIcon(R.drawable.ic___state___info___black, R.color.DEFAULT, R.color.DANGER)
                                    .setTitle("캡쳐").setMessage("얼굴을 화면의 가운데로 위치하세요.")
                                    .setButton1("다시하기", R.color.PRIMARY_TEXT, R.color.PRIMARY, (view)->{
                                        mCameraView.startCamera(CameraCapture___Activity.this);
                                    })
                                    .setButton2("그만하기", R.color.PRIMARY_TEXT, R.color.PRIMARY, (view)->{
                                        finish();
                                    })
                                    .show();
                        }
                    });
            }
        }).start();

    }
}
